<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function () {

	Route::resource('clientes', 'ClientesController');
	Route::resource('produtos', 'ProdutosController');
	Route::resource('pedidos', 'PedidosController');

});

Route::get('/trocar-linguagem/{locale}', ['as' => 'trocarlinguagem', 'uses' => 'HomeController@trocarLinguagem']);

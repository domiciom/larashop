<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\StorePedidos;
use App\Pedidos;
use App\Clientes;
use App\Produtos;

class PedidosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pedidos = Pedidos::all();
        return view('pedidos.index',compact('pedidos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $status   = Pedidos::getStatus();
        $clientes = Clientes::pluck('nomeCliente', 'id')->toArray();
        $produtos = Produtos::pluck('nomeProduto', 'id')->toArray();
        return view('pedidos.create', compact('clientes', 'produtos', 'status'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePedidos $request)
    {
       $pedido = $request->all();
       Pedidos::create($pedido);
       return redirect('pedidos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pedido=Pedidos::find($id);
        return view('pedidos.show',compact('pedido'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pedido     = Pedidos::find($id);
        $status     = Pedidos::getStatus();
        $clientes   = Clientes::pluck('nomeCliente', 'id')->toArray();
        $produtos   = Produtos::pluck('nomeProduto', 'id')->toArray();
        return view('pedidos.edit',compact('pedido','produtos', 'clientes', 'status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StorePedidos $request, $id)
    {
       $pedidoUpdate   = $request->all();
       $pedido         = Pedidos::find($id);
       $pedido->update($pedidoUpdate);

       return redirect('pedidos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Pedidos::find($id)->delete();

        return redirect('pedidos');
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProdutos extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'CodBarras'     => 'required|max:20', 
            'nomeProduto'   => 'nullable|max:100', 
            'ValorUnitario' => 'required|regex:/^\d{1,4}(\.\d{1,2})?$/',
        ];
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Produtos;
use App\Clientes;

class Pedidos extends Model
{
    protected $table 	= "pedidos";
    protected $fillable = ['id_produto', 'id_cliente', 'status', 'Quantidade', 'DtPedido'];
    // protected $touches 	= ['Produtos', 'Clientes'];

    public function Produto(){
        return Produtos::find($this->id_produto)->nomeProduto;
    }

    public function Cliente(){
        return Clientes::find($this->id_cliente)->nomeCliente;
    }

    static function getStatus(){
        return ['Em Aberto'=>trans('messages.Em Aberto'),'Pago'=> trans('messages.Pago'),'Cancelado'=> trans('messages.Cancelado')];
    }
}

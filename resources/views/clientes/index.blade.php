@extends('layouts/app')

@section('content')
 <h1>{{ trans('messages.Clients') }}</h1>
 <a href="{{url('/clientes/create')}}" class="btn btn-success">{{ trans('messages.Create')." ".trans('messages.Client') }}</a>
 <hr>
 <table class="table table-striped table-bordered table-hover">
     <thead>
     <tr class="bg-info">
         <th>Id</th>
         <th>{{ trans('messages.CPF') }}</th>
         <th>{{ trans('messages.Name') }}</th>
         <th>{{ trans('messages.Email') }}</th>
         <th class="no-filter"></th>
         <th class="no-filter"></th>
         <th class="no-filter"></th>
     </tr>
     </thead>
     <tbody>
     @foreach ($clientes as $cliente)
         <tr>
             <td>{{ $cliente->id }}</td>
             <td>{{ $cliente->cpf }}</td>
             <td>{{ $cliente->nomeCliente }}</td>
             <td>{{ $cliente->email }}</td>
             <td>
                <a href="{{url('clientes',$cliente->id)}}" class="btn btn-primary">{{ trans('messages.Read') }}</a>
             </td>
             <td>
                <a href="{{route('clientes.edit',$cliente->id)}}" class="btn btn-warning">{{ trans('messages.Update') }}</a>
             </td>
             <td>
                 {!! Form::open(['method' => 'DELETE', 'route'=>['clientes.destroy', $cliente->id]]) !!}
                 {!! Form::submit(trans('messages.Delete'), ['class' => 'btn btn-danger']) !!}
                 {!! Form::close() !!}                 
             </td>
         </tr>
     @endforeach

     </tbody>

 </table>
@endsection
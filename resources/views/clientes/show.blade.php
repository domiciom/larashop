@extends('layouts.app')
@section('content')
    <h1>Cliente mostrar</h1>

    <form class="form-horizontal">
        <div class="form-group">
            <label for="cpf" class="col-sm-2 control-label">CPF</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="cpf" placeholder="{{$cliente->cpf}}" readonly>
            </div>
        </div>
        <div class="form-group">
            <label for="NomeCliente" class="col-sm-2 control-label">Nome</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="NomeCliente" placeholder="{{$cliente->nomeCliente}}" readonly>  
            </div>
        </div>
        <div class="form-group">
            <label for="email" class="col-sm-2 control-label">E-mail</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="email" placeholder="{{$cliente->email}}" readonly>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <a href="{{ url('clientes')}}" class="btn btn-primary">Back</a>
            </div>
        </div>
    </form>
@stop
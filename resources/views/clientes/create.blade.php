@extends('layouts.app')
@section('content')
    <h1>{{ trans('messages.Create')." ".trans('messages.Client') }}</h1>
    {!! Form::open(['method' => 'POST', 'url' => 'clientes']) !!}
    <div class="form-group">
        {!! Form::label('cpf', trans('messages.CPF').':') !!}
        {!! Form::text('cpf',null,['class'=>'form-control required']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('nomeCliente', trans('messages.Name').':') !!}
        {!! Form::text('nomeCliente',null,['class'=>'form-control required']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('email', trans('E-mail').':') !!}
        {!! Form::email('email',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-sm-6">
                {!! Form::submit(trans('messages.Save'), ['class' => 'btn btn-primary form-control']) !!}
            </div>
            <div class="col-sm-6">
                <a href="{{ url('clientes')}}" class="btn btn-default form-control">{{ trans('messages.Back') }}</a>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@stop
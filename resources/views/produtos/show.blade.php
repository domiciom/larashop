@extends('layouts.app')
@section('content')
    <h1>{{ trans('messages.Show')." ".trans('messages.Product') }}</h1>

    <form class="form-horizontal">
        <div class="form-group">
            <label for="codbarra" class="col-sm-2 control-label">{{ trans('messages.Barcode') }}</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="codbarra" placeholder="{{$produto->CodBarras}}" readonly>
            </div>
        </div>
        <div class="form-group">
            <label for="nomeProduto" class="col-sm-2 control-label">{{ trans('messages.Name') }}</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="nomeProduto" placeholder="{{$produto->nomeProduto}}" readonly>  
            </div>
        </div>
        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">{{ trans('messages.Value') }}</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="title" placeholder="{{$produto->ValorUnitario}}" readonly>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <a href="{{ url('produtos')}}" class="btn btn-primary">{{ trans('messages.Back') }}</a>
            </div>
        </div>
    </form>
@stop
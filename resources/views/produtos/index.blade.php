@extends('layouts/app')

@section('content')
 <h1>{{ trans('messages.Products') }}</h1>
 <a href="{{url('/produtos/create')}}" class="btn btn-success">{{ trans('messages.Create')." ".trans('messages.Product')}}</a>
 <hr>
 <table class="table table-striped table-bordered table-hover">
     <thead>
     <tr class="bg-info">
         <th>Id</th>
         <th>{{ trans('messages.Barcode') }}</th>
         <th>{{ trans('messages.Name')}}</th>
         <th>{{ trans('messages.Value') }}</th>
         <th class="no-filter"></th>
         <th class="no-filter"></th>
         <th class="no-filter"></th>
     </tr>
     </thead>
     <tbody>
     @foreach ($produtos as $produto)
         <tr>
             <td>{{ $produto->id }}</td>
             <td>{{ $produto->CodBarras }}</td>
             <td>{{ $produto->nomeProduto }}</td>
             <td>{{ trans('messages.currency').$produto->ValorUnitario }}</td>
             <td>
                <a href="{{url('produtos',$produto->id)}}" class="btn btn-primary">{{ trans('messages.Read') }}</a>
             </td>
             <td>
                <a href="{{route('produtos.edit',$produto->id)}}" class="btn btn-warning">{{ trans('messages.Update') }}</a>
             </td>
             <td>
                 {!! Form::open(['method' => 'DELETE', 'route'=>['produtos.destroy', $produto->id]]) !!}
                 {!! Form::submit(trans('messages.Delete'), ['class' => 'btn btn-danger']) !!}
                 {!! Form::close() !!}                 
             </td>
         </tr>
     @endforeach

     </tbody>
 </table>
@endsection
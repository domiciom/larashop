@extends('layouts.app')
@section('content')
    <h1>{{ trans('messages.Create')." ".trans('messages.Products') }}</h1>
    {!! Form::open(['method' => 'POST', 'url' => 'produtos']) !!}
    <div class="form-group">
        {!! Form::label('CodBarras', trans('messages.Barcode').':') !!}
        {!! Form::text('CodBarras',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('nomeProduto', trans('messages.Name').':') !!}
        {!! Form::text('nomeProduto',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('ValorUnitario', trans('messages.Value').':') !!}
        {!! Form::text('ValorUnitario',null,['class'=>'form-control', 'id'=>'ValorUnitario', 'placeholder'=>'0.00']) !!}
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-sm-6">
                {!! Form::submit(trans('messages.Save'), ['class' => 'btn btn-primary form-control']) !!}
            </div>
            <div class="col-sm-6">
                <a href="{{ url('produtos')}}" class="btn btn-default form-control">{{ trans('messages.Back') }}</a>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@stop
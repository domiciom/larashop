@extends('layouts.app')
@section('content')
    <h1>{{trans ('messages.Update')." ".trans('messages.Product') }}</h1>
    {!! Form::model($produto,['method' => 'PATCH','route'=>['produtos.update',$produto->id]]) !!}
    <div class="form-group">
        {!! Form::label('CodBarras', trans('messages.Barcode').':') !!}
        {!! Form::text('CodBarras',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('nomeProduto', trans('messages.Name').':') !!}
        {!! Form::text('nomeProduto',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('ValorUnitario', trans('messages.Value').':') !!}
        {!! Form::text('ValorUnitario',null,['class'=>'form-control', 'id'=>'ValorUnitario', 'placeholder'=>'0.00']) !!}
    </div>
    <div class="form-group">
        {!! Form::submit(trans('messages.Update'), ['class' => 'btn btn-primary']) !!}
         <a href="{{ url('produtos')}}" class="btn btn-default">{{ trans('messages.Back') }}</a>
    </div>
    {!! Form::close() !!}
@stop
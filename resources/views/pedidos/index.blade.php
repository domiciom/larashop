@extends('layouts/app')

@section('content')
 <h1>{{ trans('messages.Orders') }}</h1>
 <a href="{{url('/pedidos/create')}}" class="btn btn-success">{{trans('messages.Create')." ".trans('messages.Order') }}</a>
 <hr>
 <div class="table-responsive">
     <table class="table table-striped table-bordered" width="100%">
         <thead>
             <tr class="bg-info">
                <th>Id</th>
                <th>{{ trans('messages.Product') }}</th>
                <th>{{ trans('messages.Client') }}</th>
                <th>{{ trans('messages.Status') }}</th>
                <th>{{ trans('messages.Amount') }}</th>
                <th>{{ trans('messages.DtPedido') }}</th>
                <th class="no-filter"> </th>
                <th class="no-filter"> </th>
                <th class="no-filter"> </th>
             </tr>
         </thead>
         <tbody>
         @foreach ($pedidos as $pedido)
             <tr>
                 <td>{{ $pedido->id }}</td>
                 <td>{{ $pedido->Produto() }}</td>
                 <td>{{ $pedido->Cliente() }}</td>
                 <td>{{ trans('messages.'.$pedido->status) }}</td>
                 <td>{{ $pedido->Quantidade }}</td>
                 <td>{{ $pedido->DtPedido }}</td>
                <td>
                    <a href="{{url('pedidos',$pedido->id)}}" class="btn btn-primary">{{ trans('messages.Read') }}</a>
                 </td>
                 <td>
                    <a href="{{route('pedidos.edit',$pedido->id)}}" class="btn btn-warning">{{ trans('messages.Update') }}</a>
                 </td>
                 <td>
                     {!! Form::open(['method' => 'DELETE', 'route'=>['pedidos.destroy', $pedido->id]]) !!}
                     {!! Form::submit(trans('messages.Delete'), ['class' => 'btn btn-danger']) !!}
                     {!! Form::close() !!}                 
                 </td>
             </tr>
         @endforeach
         </tbody>
     </table>

 </div>
@endsection
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_produto');
            $table->integer('id_cliente');
            $table->enum('status', ['Em Aberto', 'Pago', 'Cancelado'])->default('Em Aberto');
            $table->integer('Quantidade');
            $table->dateTime('DtPedido');
        });


        $this->adicionaClienteProduto();    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->dropForeign('pedidos_id_produto_foreign');
        $table->dropForeign('pedidos_id_cliente_foreign');
        Schema::drop('pedidos'); 
    }

    public function adicionaClienteProduto(){
        if(Schema::hasTable('pedidos') && Schema::hasColumn('id_produto', 'id_cliente')){
            Schema::table('posts', function (Blueprint $table) {
                $table->integer('id_produto')->unsigned();
                $table->foreign('id_produto')->references('id')->on('produtos');

                $table->integer('id_cliente')->unsigned();
                $table->foreign('id_cliente')->references('id')->on('clientes');
            });   
        }
    }
}
